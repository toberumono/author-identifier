package authorIdentifier;

public class Pair<T, V> {
	private final T first;
	private final V last;
	
	public Pair(T first, V last) {
		this.first = first;
		this.last = last;
	}
	
	public T getFirst() {
		return first;
	}
	
	public V getLast() {
		return last;
	}
}
