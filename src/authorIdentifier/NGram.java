package authorIdentifier;

import java.util.HashMap;
import java.util.Set;

public class NGram {
	private HashMap<String, Double> words;
	private int numOccurances;
	
	public NGram() { // All NGrams starting with the same word are stored in an 'NGram'
		words = new HashMap<String, Double>(); // words -- the possible 'continuations' of the NGram's first word; each is stored with the count of occurrences
		words.put("<unknown>", 0.0);
		numOccurances = 0;
	}
	
	public Set<String> getAllWords() {
		return words.keySet();
	}
	
	public void addWord(String word) {
		words.put(word, words.containsKey(word) ? words.get(word) + 1 : 1);
		numOccurances++;
	}
	
	public void addWord(String word, int count) {
		words.put(word, words.containsKey(word) ? words.get(word) + count : count);
		numOccurances += count;
	}
	
	public void combine(NGram nGram) { // ???? adds new word sequence to a word that already begins other NGrams
		for (String word : nGram.words.keySet())
			words.put(word, words.containsKey(word) ? words.get(word) + nGram.words.get(word) : nGram.words.get(word));
		numOccurances += nGram.numOccurances;
	}
	
	public double probability(String word) {
		return words.containsKey(word) ? words.get(word) / numOccurances : 1.0 / (numOccurances + 1.0);
	}
	
	public HashMap<String, Double> getNextWords() {
		return words;
	}
	
	@Override
	public String toString() {
		String output = "Total words: " + numOccurances + ": ";
		for (String word : words.keySet())
			output = output + word + " : " + words.get(word) + ", ";
		return output.endsWith(", ") ? output.substring(0, output.length() - 2) : output;
	}
}
