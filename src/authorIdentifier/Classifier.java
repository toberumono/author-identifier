package authorIdentifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lexer.Action;
import lexer.Lexer;
import lexer.Rule;
import lexer.Token;
import lexer.Type;
import lexer.errors.LexerException;
import authorIdentifier.smoothers.GoodTuring;
import authorIdentifier.smoothers.Smoother;

public class Classifier {
	private HashMap<String, ArrayList<String>> testing;
	private HashMap<String, Author> authors;
	private final Lexer lexer = new Lexer();
	
	public Classifier() {
		testing = new HashMap<String, ArrayList<String>>();
		authors = new HashMap<String, Author>();
		lexer.addRule("Word or Number", new Rule<String>(Pattern.compile("_*(([\\w&&[^_0-9]]+((-|')[\\w&&[^_0-9]]+)*)|-{2,}|[0-9]+(,[0-9])*(\\.[0-9]+(,[0-9])*)?('?s)?|\\.[0-9]+(,[0-9])*('?s)?)_*", Pattern.CASE_INSENSITIVE),
				new Type<String>("word"), new Action<String>() {
					
					@Override
					public String action(String input, Lexer lexer) {
						return input.replaceAll("_", "").replaceAll("(([sS])'|'[sS])", "$2"); //Removes possessives, correctly handles plurals
					}
				}));
		//Add abbreviations here
		lexer.addRule("Abbreviation", new Rule<String>(Pattern.compile("(Mr\\.|Mrs\\.|Jan\\.|Feb\\.|Mar\\.|Apr\\.|Jun\\.|Jul\\.|Aug\\.|Sep\\.|Oct\\.|Nov\\.|Dec\\.)"), new Type<String>("word"), null));
		//Add separators that wouldn't count as a break in an n-gram here
		lexer.addRule("Separators", new Rule<String>(Pattern.compile("[\\Q.?!\\E]"), new Type<String>("separator"), null));
		lexer.addRule("Punctuation", new Rule<String>(Pattern.compile("\\W"), new Type<String>("punctuation"), null));
	}
	
	/**
	 * Takes the given line and returns an <tt>ArrayList</tt> of the sentences where each sentence is an array of the words
	 * in that sentence.
	 * 
	 * @param line
	 *            the line to parse as a <tt>String</tt>
	 * @return the sentences and the words in those sentences in the given line
	 */
	private ArrayList<String> parseLine(String line, String author, boolean addTesting) {
		if ((line = line.trim()).length() == 0)
			return new ArrayList<String>();
		Token input;
		try {
			input = lexer.lex(line); // input -- the text to be parsed
		}
		catch (LexerException e) {
			e.printStackTrace();
			return new ArrayList<String>();
		}
		if (addTesting && !testing.containsKey(author)) // associate author with testing set, if necessary
			testing.put(author, new ArrayList<String>());
		ArrayList<String> output = new ArrayList<String>(); // output -- the sentences for the training set
		do {
			Token sentence = input.singular(); // what does this do?
			while (!(input = input.getNextToken()).isNull()) {
				sentence = sentence.append(input.singular());
				if (input.getCarType().equals("separator")) // break text on sentence-ending punctuation
					break;
			}
			sentence = sentence.getFirstToken(); // ???
			if (addTesting && (int) (Math.random() * 10) == 0) {
				testing.get(author).add(sentence.toString()); // randomly add 10% of sentences to test set, if necessary
				continue;
			}
			do {
				output.add((String) sentence.getCar()); // ????
			} while (!(sentence = sentence.getNextToken()).isNull());
		} while (!(input = input.getNextToken()).isNull());
		return output;
	}
	
	private Author makeAuthor(File file, int N, String author) throws FileNotFoundException {
		Author auth = author != null && authors.containsKey(author) ? authors.get(author) : new Author();
		Scanner lines = new Scanner(file);
		Pattern chapter = Pattern.compile("(((chapter|book) )?([0-9]+|[ivxlc]+)\\.?|( +|\\*+)+)");
		while (lines.hasNextLine()) {
			String line = lines.nextLine().toLowerCase();
			Matcher m = chapter.matcher(line);
			//If it is a header, skip it
			if (m.find() && m.start() == 0)
				continue;
			ArrayList<String> words = parseLine(line, author, author != null);
			if (words.size() < N) {
				for (String word : words)
					auth.addVocabWord(word);
				continue; // if a line is less than one nGram long, add words just to vocabulary, not as nGram
			}
			for (int i = 0; i < N - 1; i++)
				auth.addVocabWord(words.get(i));
			for (int i = N - 1; i < words.size(); i++) {
				auth.addVocabWord(words.get(i));
				if (N > 1) {
					String nGram = "";
					for (int j = i - N + 1; j < i; j++)
						// grab previous N-1 words
						nGram = nGram + " " + words.get(j);
					nGram = nGram.substring(1); //Remove the leading space
					auth.addNGram(nGram, words.get(i));
				}
			}
		}
		lines.close();
		if (author != null)
			authors.put(author, auth);
		return auth;
	}
	
	public void loadFile(String author, String path, int N) throws FileNotFoundException {
		File file = new File(path);
		makeAuthor(file, N, author);
	}
	
	public void smooth(Smoother smoother) {
		for (String author : authors.keySet())
			authors.put(author, smoother.smooth(authors.get(author))); // update counts for each author given a particular smoothing algorithm
	}
	
	private ArrayList<Pair<String, String>> toNGrams(String input, int N) {
		if (N < 2)
			return new ArrayList<Pair<String, String>>();
		ArrayList<String> words = parseLine(input, null, false);
		ArrayList<Pair<String, String>> output = new ArrayList<Pair<String, String>>();
		for (int i = N - 1; i < words.size(); i++) {
			String nGram = "";
			for (int j = i - N + 1; j < i; j++)
				nGram = nGram + " " + words.get(j);
			nGram = nGram.substring(1); //Remove the leading space
			output.add(new Pair<String, String>(nGram, words.get(i)));
		}
		return output;
	}
	
	public String classify(ArrayList<String> lines, int N) {
		return classify(lines, N, true);
	}
	
	public String classify(ArrayList<String> lines, int N, boolean showProbabilities) {
		ArrayList<ArrayList<Pair<String, String>>> lineNGrams = new ArrayList<ArrayList<Pair<String, String>>>();
		for (String line : lines)
			lineNGrams.add(toNGrams(line.toLowerCase(), N));
		Double bestOff = null;
		String likelyAuthor = "";
		for (String auth : authors.keySet()) {
			double off = 0;
			Author a = authors.get(auth);
			for (ArrayList<Pair<String, String>> line : lineNGrams)
				off += a.probability(line);
			off = off / lineNGrams.size() * Integer.signum((int) off); //Get the average probability that a given line in this test set was produced by this grammar
			if (showProbabilities)
				System.out.println(auth + " : " + off);
			if (bestOff == null || off < bestOff) {
				bestOff = off;
				likelyAuthor = auth;
			}
		}
		return likelyAuthor;
	}
	
	public static void main(String[] args) {
		Classifier classifier = new Classifier();
		int N = 2;
		try {
			classifier.loadFile("austen", "Training/austen.txt", N);
			classifier.loadFile("christie", "Training/christie.txt", N);
			classifier.loadFile("dickens", "Training/dickens.txt", N);
			classifier.loadFile("doyle", "Training/doyle.txt", N);
			classifier.loadFile("grimm", "Training/grimm.txt", N);
			classifier.loadFile("thoreau", "Training/thoreau.txt", N);
			classifier.loadFile("whitman", "Training/whitman.txt", N);
			classifier.loadFile("wilde", "Training/wilde.txt", N);
			classifier.smooth(new GoodTuring());
			if (args.length > 0) {
				String input = "";
				for (String arg : args)
					input = input + " " + arg;
				input = input.substring(1);
				ArrayList<String> inp = new ArrayList<String>();
				inp.add(input);
				System.out.println(classifier.classify(inp, N, true));
			}
			else {
				System.out.println("austen : " + classifier.classify(classifier.testing.get("austen"), N) + "\n");
				System.out.println("christie : " + classifier.classify(classifier.testing.get("christie"), N) + "\n");
				System.out.println("dickens : " + classifier.classify(classifier.testing.get("dickens"), N) + "\n");
				System.out.println("doyle : " + classifier.classify(classifier.testing.get("doyle"), N) + "\n");
				System.out.println("grimm : " + classifier.classify(classifier.testing.get("grimm"), N) + "\n");
				System.out.println("thoreau : " + classifier.classify(classifier.testing.get("thoreau"), N) + "\n");
				System.out.println("whitman : " + classifier.classify(classifier.testing.get("whitman"), N) + "\n");
				System.out.println("wilde : " + classifier.classify(classifier.testing.get("wilde"), N) + "\n");
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
