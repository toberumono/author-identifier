package authorIdentifier;

import java.util.ArrayList;
import java.util.HashMap;

public class Author {
	private final HashMap<String, NGram> nGrams;
	private final HashMap<String, Double> vocabulary;
	private int vocabularySize;
	
	public Author(HashMap<String, NGram> nGrams, HashMap<String, Double> vocabulary, int vocabularySize) {
		this.nGrams = nGrams;
		this.vocabulary = vocabulary;
		this.vocabularySize = vocabularySize;
	}
	
	// an author's name is stored with their total vocabulary and unigram counts, and total NGrams and NGram counts
	public Author() {
		nGrams = new HashMap<String, NGram>();
		vocabulary = new HashMap<String, Double>();
		vocabularySize = 0;
	}
	
	public void addNGram(String nGram, String word) {
		if (nGrams.containsKey(nGram))
			nGrams.get(nGram).addWord(word);
		else {
			NGram n = new NGram();
			n.addWord(word);
			nGrams.put(nGram, n);
		}
	}
	
	public void addVocabWord(String word) {
		vocabularySize++;
		vocabulary.put(word, vocabulary.containsKey(word) ? vocabulary.get(word) + 1 : 1);
	}
	
	public double probability(ArrayList<Pair<String, String>> nGrams) {
		if (nGrams.size() == 0) //If there are no words, the probability that this author wrote them is zero.
			return 0;
		double probability = 0;
		for (Pair<String, String> nGram : nGrams)
			probability += this.nGrams.containsKey(nGram.getFirst()) ? Math.log10(this.nGrams.get(nGram.getFirst()).probability(nGram.getLast())) : Math.log10(1.0 / (vocabularySize + 1.0));
		return probability / nGrams.size();
	}
	
	public HashMap<String, NGram> getNGrams() {
		return nGrams;
	}
	
	public HashMap<String, Double> getVocabulary() {
		return vocabulary;
	}
	
	public NGram getNGram(String nGram) {
		return nGrams.get(nGram);
	}
	
	public int getVocabularySize() {
		return vocabularySize;
	}
	
	public double getVocabularyCount(String word) {
		return vocabulary.get(word);
	}
	
	public double[] getFrequencies() {
		
		double[] frequencies = new double[6];
		frequencies[0] = vocabularySize * vocabularySize; // frequency of 0 
		
		for (String nGram : nGrams.keySet()) {
			NGram startWord = nGrams.get(nGram);
			for (String word : startWord.getNextWords().keySet())
				// for all the words that follow that nGram
				if (startWord.getNextWords().get(word).intValue() < 6)
					frequencies[startWord.getNextWords().get(word).intValue()] += 1;
		}
		
		return frequencies;
	}
	
	@Override
	public String toString() {
		return nGrams.toString();
	}
}
