package authorIdentifier.smoothers;

import java.util.HashMap;

import authorIdentifier.Author;
import authorIdentifier.NGram;

public class Interpolation implements Smoother{

	@Override
	public Author smooth(Author author) {
		HashMap<String, NGram> nGrams = author.getNGrams();
		HashMap<String, Double> vocabulary = author.getVocabulary();
		
		//TODO Do stuff
		
		return new Author(nGrams, vocabulary, author.getVocabularySize());
	}

}
