package authorIdentifier.smoothers;

import java.util.HashMap;

import authorIdentifier.Author;
import authorIdentifier.NGram;

public class Backoff implements Smoother{

	@Override
	public Author smooth(Author author) {
		HashMap<String, NGram> nGrams = author.getNGrams();
		HashMap<String, Double> vocabulary = author.getVocabulary();
		
		for (String nGram : nGrams.keySet()) {
			NGram startWord = nGrams.get(nGram);
			for (String word : startWord.getNextWords().keySet()){ // for all the words that follow that nGram
				Double c = startWord.getNextWords().get(word);
				Double cStar;
			}
		}
		return new Author(nGrams, vocabulary, author.getVocabularySize());
	}

}
