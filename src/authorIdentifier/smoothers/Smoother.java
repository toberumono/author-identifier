package authorIdentifier.smoothers;

import authorIdentifier.Author;

public interface Smoother {
	
	public Author smooth(Author author);
}
