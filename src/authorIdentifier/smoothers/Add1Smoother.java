package authorIdentifier.smoothers;

import java.util.HashMap;
import java.util.Set;

import authorIdentifier.Author;
import authorIdentifier.NGram;

public class Add1Smoother implements Smoother {
	
	@Override
	public Author smooth(Author author) {
		HashMap<String, NGram> nGrams = author.getNGrams();
		HashMap<String, Double> vocabulary = author.getVocabulary();
		for (String nGram : nGrams.keySet()) {
			NGram ng = nGrams.get(nGram);
			Set<String> words = ng.getAllWords();
			for (String word : words)
				ng.addWord(word);
		}
		return new Author(nGrams, vocabulary, author.getVocabularySize());
	}
}
