package authorIdentifier.smoothers;

import java.util.HashMap;

import authorIdentifier.Author;
import authorIdentifier.NGram;

public class GoodTuring implements Smoother {

	@Override
	public Author smooth(Author author) {
		HashMap<String, NGram> nGrams = author.getNGrams();
		HashMap<String, Double> vocabulary = author.getVocabulary();
		double[] frequencies = author.getFrequencies();
		int cap = 0;
		for (; cap < 5; cap++)
			if (frequencies[cap] == 0)
				break;
		for (String nGram : nGrams.keySet()) {
			NGram startWord = nGrams.get(nGram);
			for (String word : startWord.getNextWords().keySet()) { // for all the words that follow that nGram
				Double c = startWord.getNextWords().get(word);
				if (c.intValue() < cap)
					startWord.getNextWords().put(word, (c + 1) * (frequencies[c.intValue() + 1] / frequencies[c.intValue()]));
			}
		}
		
		return new Author(nGrams, vocabulary, author.getVocabularySize());
	}

}


