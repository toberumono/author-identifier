package stemmer;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lexer.Action;
import lexer.Descender;
import lexer.Lexer;
import lexer.Rule;
import lexer.Token;
import lexer.Type;
import lexer.errors.LexerException;

public class Stemmer {
	private static final Pattern consonant = Pattern.compile("([^aeiouy]|(?<![^aeiouy])y)", Pattern.CASE_INSENSITIVE),
			vowel = Pattern.compile("([aeiou]|(?<![aeiou])y)", Pattern.CASE_INSENSITIVE),
			CONSONANT = Pattern.compile("([^aeiouy]|(?<![^aeiouy])y)+", Pattern.CASE_INSENSITIVE),
			measure = Pattern.compile("([aeiou]|(?<![aeiou])y)+([^aeiouy]|(?<![^aeiouy])y)+", Pattern.CASE_INSENSITIVE);
	
	private static final Lexer lexer = new Lexer();
	private static boolean initialized = false;
	private static ArrayList<ArrayList<Token>> rules = new ArrayList<ArrayList<Token>>();
	private static final Type<Boolean> booleanType = new Type<Boolean>("boolean");
	
	public static String stem(String word) {
		if (!initialized)
			init();
		boolean step11or12 = false;
		for (int i = 0; i < rules.size(); i++) {
			String replacement = null;
			int longestMatch = -1;
			ArrayList<Token> set = rules.get(i);
			for (int j = 0; j < set.size(); j++) {
				Token rule = set.get(j), last = rule.getLastToken(), prev = last.getPreviousToken();
				String match = (String) (last.getCarType().equals("output") ? (prev.getCarType().equals(Type.TOKEN) ? "" : prev.getCar()) :
						(prev = prev.getPreviousToken()).getCarType().equals(Type.TOKEN) ? "" : prev.getCar());
				if (!word.toLowerCase().endsWith(match.toLowerCase()) || (rule.getCarType().equals(Type.TOKEN) && !satisfiesRule(word.substring(0, word.length() - match.length()), (Token) rule.getCar())) ||
						(longestMatch > -1 && match.length() <= longestMatch))
					continue;
				step11or12 = i == 1 && (j == 1 || j == 2);
				longestMatch = match.length();
				replacement = (String) (last.getCarType().getName().equals("output") ? "" : last.getCar());
			}
			//If replacement == null, no rules match this word
			if (replacement == null)
				continue;
			if (replacement.equals("{LEAVE ONE}"))
				word = word.substring(0, word.length() - 1);
			else
				word = word.substring(0, word.length() - longestMatch) + replacement.toLowerCase();
			if (i == 1 && !step11or12)
				i++;
		}
		return word;
	}
	
	private static final boolean satisfiesRule(String stem, Token rule) {
		try {
			return satisfiesRuleInner(stem, lexer.lex(rule.toString()));
		}
		catch (LexerException e) {
			return false;
		}
	}
	
	/**
	 * Checks if the given stem satisfies the given conditions
	 * 
	 * @param stem
	 *            the stem
	 * @param head
	 *            the conditions
	 * @return whether stem satisfies the conditions
	 */
	private static final boolean satisfiesRuleInner(String stem, Token head) {
		Token rule = head;
		do {
			//Special conditions
			if (head.getCarType().equals("patternCheck"))
				head.replaceCar(new Token(evalCondition(stem, (String) head.getCar()), booleanType));
			//m > 0 type stuff
			else if (head.getCarType().equals("comparator")) {
				Token prev = head.getPreviousToken(), next = head.getNextToken();
				int first = 0, second = 0;
				if (prev.getCarType().equals("integer"))
					first = (int) prev.getCar();
				else
					first = measure(stem);
				if (next.getCarType().equals("integer"))
					second = (int) next.getCar();
				else
					second = measure(stem);
				boolean result = false;
				if (((String) head.getCar()).startsWith(">")) {
					if (((String) head.getCar()).endsWith("="))
						result = first >= second;
					else
						result = first > second;
				}
				else if (((String) head.getCar()).startsWith("<")) {
					if (((String) head.getCar()).endsWith("="))
						result = first <= second;
					else
						result = first < second;
				}
				else if (((String) head.getCar()).endsWith("=")) {
					if (((String) head.getCar()).startsWith("!"))
						result = first != second;
					else
						result = first == second;
				}
				prev.replaceCar(new Token(result, booleanType));
				prev.append(next.getNextToken());
				head = prev;
			}
			//parenthesized conditions
			else if (head.getCarType().equals(Type.TOKEN))
				head.replaceCar(new Token(satisfiesRuleInner(stem, (Token) head.getCar()), booleanType));
		} while (!(head = head.getNextToken()).isNull());
		head = rule;
		//At this point there should only be &&, ||, True, False, or not in the rule
		for (int i = 0; i < 3; i++) {
			if (head.getCarType().equals("operator") || head.getCarType().equals("negate"))
				do {
					if (i == 0 && head.getCarType().equals("negate")) {
						head.replaceCar(new Token(!(boolean) head.getNextToken().getCar(), booleanType));
						head.append(head.getNextToken().getNextToken());
						continue;
					}
					else if (i == 1 && head.getCar().equals("&&")) {
						head = head.getPreviousToken();
						head.replaceCar(new Token((boolean) head.getCar() && (boolean) head.getNextToken().getNextToken().getCar(), booleanType));
						head.append(head.getNextToken().getNextToken().getNextToken());
					}
					else if (i == 2 && head.getCar().equals("||")) {
						head = head.getPreviousToken();
						head.replaceCar(new Token((boolean) head.getCar() || (boolean) head.getNextToken().getNextToken().getCar(), booleanType));
						head.append(head.getNextToken().getNextToken().getNextToken());
					}
				} while ((head = head.getNextToken()).getCarType().equals("operator") || head.getCarType().equals("negate"));
			head = rule;
		}
		return (boolean) head.getCar();
	}
	
	/**
	 * This must be called with a valid condition
	 * 
	 * @param word
	 *            the word to check
	 * @param condition
	 *            the condition to check with
	 * @return the result of checking the condition
	 */
	private static final boolean evalCondition(String word, String condition) {
		if (condition.equals("*v*"))
			return vowel.matcher(word).find();
		if (condition.equals("*d")) {
			if (word.length() < 3)
				return false;
			word = word.substring(word.length() - 3);
			Matcher m = vowel.matcher(word);
			if (m.find() && m.start() == 0 && (word.charAt(1) == 'y' || word.charAt(1) == 'Y'))
				return false;
			word = word.substring(1);
			return CONSONANT.matcher(word).matches();
		}
		if (condition.equals("*o")) {
			if (word.length() < 4)
				return false;
			word = word.substring(word.length() - 4).toLowerCase();
			Matcher m = consonant.matcher(word);
			if (m.find() && m.start() == 1 && m.find() && m.start() == 3 && !(word.charAt(3) == 'w' || word.charAt(3) == 'x' || word.charAt(3) == 'y'))
				return (m = vowel.matcher(word)).find() && m.start() == 2;
		}
		return word.toLowerCase().endsWith(condition.substring(1).toLowerCase());
	}
	
	public static final void addRule(int set, String rule) {
		if (!initialized)
			init();
		while (rules.size() <= set)
			rules.add(new ArrayList<Token>());
		try {
			rules.get(set).add(lexer.lex(rule));
		}
		catch (LexerException e) {}
	}
	
	//initializes a series of rules
	private static final void init() {
		initialized = true;
		initLexerRules();
		addRule(0, "SSES -> SS");
		addRule(0, "IES -> S");
		addRule(0, "SS -> SS");
		addRule(0, "S -> ");
		addRule(1, "(m > 0) EED -> EE");
		addRule(1, "(*v*) ED -> ");
		addRule(1, "(*v*) ING -> ");
		addRule(2, "AT -> ATE");
		addRule(2, "BL -> BLE");
		addRule(2, "IZ -> IZE");
		addRule(2, "(*d and not (*L or *S or *Z)) -> {LEAVE ONE}");
		addRule(2, "(m = 1 and *o) -> E");
		addRule(3, "(*v*) Y -> I");
		addRule(4, "(m > 0) ATIONAL -> ATE");
		addRule(4, "(m > 0) TIONAL -> TION");
		addRule(4, "(m > 0) ENCI -> ENCE");
		addRule(4, "(m > 0) ANCI -> ANCE");
		addRule(4, "(m > 0) IZER -> IZE");
		addRule(4, "(m > 0) ABLI -> ABLE");
		addRule(4, "(m > 0) ALLI -> AL");
		addRule(4, "(m > 0) ENTLI -> ENT");
		addRule(4, "(m > 0) ELI -> E");
		addRule(4, "(m > 0) OUSLI -> OUS");
		addRule(4, "(m > 0) IZATION -> IZE");
		addRule(4, "(m > 0) ATION -> ATE");
		addRule(4, "(m > 0) ATOR -> ATE");
		addRule(4, "(m > 0) ALISM -> AL");
		addRule(4, "(m > 0) IVENESS -> IVE");
		addRule(4, "(m > 0) FULNESS -> FUL");
		addRule(4, "(m > 0) OUSNESS -> OUS");
		addRule(4, "(m > 0) ALITI -> AL");
		addRule(4, "(m > 0) IVITI -> IVE");
		addRule(4, "(m > 0) BILITI -> BLE");
		addRule(5, "(m > 0) ICATE -> IC");
		addRule(5, "(m > 0) ATIVE -> ");
		addRule(5, "(m > 0) ALIZE -> AL");
		addRule(5, "(m > 0) ICITI -> IC");
		addRule(5, "(m > 0) ICAL -> IC");
		addRule(5, "(m > 0) FUL -> ");
		addRule(5, "(m > 0) NESS -> ");
		addRule(6, "(m > 1) AL -> ");
		addRule(6, "(m > 1) ANCE -> ");
		addRule(6, "(m > 1) ENCE -> ");
		addRule(6, "(m > 1) ER -> ");
		addRule(6, "(m > 1) IC -> ");
		addRule(6, "(m > 1) ABLE -> ");
		addRule(6, "(m > 1) IBLE -> ");
		addRule(6, "(m > 1) ANT -> ");
		addRule(6, "(m > 1) EMENT -> ");
		addRule(6, "(m > 1) MENT -> ");
		addRule(6, "(m > 1) ENT -> ");
		addRule(6, "(m > 1 and (*S or *T)) ION -> ");
		addRule(6, "(m > 1) OU -> ");
		addRule(6, "(m > 1) ISM -> ");
		addRule(6, "(m > 1) ATE -> ");
		addRule(6, "(m > 1) ITI -> ");
		addRule(6, "(m > 1) OUS -> ");
		addRule(6, "(m > 1) IVE -> ");
		addRule(6, "(m > 1) IZE -> ");
		addRule(7, "(m > 1) E -> ");
		addRule(7, "(m = 1 and not *o) E -> ");
		addRule(8, "(m > 1 and *d and *L) -> {LEAVE ONE}");
	}
	
	private static final void initLexerRules() {
		lexer.addDescender("parens", new Descender("(", ")", Type.TOKEN, null));
		lexer.addRule("endCharCheck", new Rule<String>(Pattern.compile("\\*[ABCDEFGHIJKLMNOPQRSTUVWXYZ]"), new Type<String>("patternCheck"), null));
		lexer.addRule("vowelCheck", new Rule<String>(Pattern.compile("\\*v\\*"), new Type<String>("patternCheck"), null));
		lexer.addRule("doubleConsonantCheck", new Rule<String>(Pattern.compile("\\*d"), new Type<String>("patternCheck"), null));
		lexer.addRule("cvcCheck", new Rule<String>(Pattern.compile("\\*o"), new Type<String>("patternCheck"), null));
		lexer.addRule("mCheck", new Rule<String>(Pattern.compile("m"), new Type<String>("patternCheck"), null));
		lexer.addRule("not", new Rule<String>(Pattern.compile("(not|\\!([^=]|\\z|$))"), new Type<String>("negate"), null));
		lexer.addRule("output", new Rule<String>(Pattern.compile("->"), new Type<String>("output"), null));
		lexer.addRule("word", new Rule<String>(Pattern.compile("\\w+"), new Type<String>("word"), null));
		lexer.addRule("specialWord", new Rule<String>(Pattern.compile("\\{LEAVE ONE\\}"), new Type<String>("word"), null));
		lexer.addRule("boolean", new Rule<Boolean>(Pattern.compile("\\{(TRUE|FALSE)\\}"), new Type<Boolean>("boolean"), new Action<Boolean>() {
			
			@Override
			public Boolean action(String input, Lexer lexer) {
				return input.equals("{TRUE}");
			}
		}));
		lexer.addRule("integer", new Rule<Integer>(Pattern.compile("[0-9]+"), new Type<Integer>("integer"), new Action<Integer>() {
			
			@Override
			public Integer action(String input, Lexer lexer) {
				return Integer.parseInt(input);
			}
		}));
		lexer.addRule("logic", new Rule<String>(Pattern.compile("(\\|\\||&&|or|and)"), new Type<String>("logic"), new Action<String>() {
			
			@Override
			public String action(String input, Lexer lexer) {
				return input.equals("and") ? "&&" : input.equals("or") ? "||" : input;
			}
		}));
		lexer.addRule("comparator", new Rule<String>(Pattern.compile("(<|>|>=|<=|!=|=)"), new Type<String>("comparator"), null));
	}
	
	/**
	 * Gets determines the measure of the word by matching the measure pattern against the word.
	 * 
	 * @param word
	 *            the word to get the measure of
	 * @return matches <= 2 ? 0 : matches - 2; This accounts for the need of a leading consonant and trailing vowel before
	 *         measuring a word.
	 */
	public static final int measure(String word) {
		Matcher m = measure.matcher(word);
		int measure = 0;
		while (m.find())
			measure++;
		return measure;
	}
	
	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		String input = "";
		System.out.println("Please enter a word to stem. (type sys:exit to quit):");
		while ((input = userInput.nextLine()).equals("sys:exit")) {
			System.out.println(stem(input));
			System.out.println("Please enter a word to stem. (type sys:exit to quit):");
		}
		userInput.close();
	}
}
