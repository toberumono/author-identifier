package lexer.errors;

public class LexerException extends Exception {

	public LexerException(String message) {
		super(message);
	}
}
